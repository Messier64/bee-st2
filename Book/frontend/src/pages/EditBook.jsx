import React, { useState, useEffect } from 'react';
import BackButton from '../components/BackButton';
import Spinner from '../components/Spinner';
import axios from 'axios';
import { useNavigate, useParams } from 'react-router-dom';
import { useSnackbar } from 'notistack';

const EditBook = () => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [genre, setGenre] = useState('');
  const [ReleaseYear, setRY] = useState('');
  const [Reviews, setRw] = useState('');


  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const {id} = useParams();
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    setLoading(true);
    axios.get(`http://localhost:5555/books/${id}`)
    .then((response) => {
        setDescription(response.data.description);
        setRY(response.data.ReleaseYear)
        setTitle(response.data.title)
        setGenre(response.data.genre)
        setRw(response.data.Reviews)
        setLoading(false);
      }).catch((error) => {
        setLoading(false);
        alert('An error happened. Please Chack console');
        console.log(error);
      });
  }, [])
  
  const handleEditBook = () => {
    const data = {
      title,
      description,
      genre,
      ReleaseYear,
      Reviews,
    };
    setLoading(true);
    axios
      .put(`http://localhost:5555/books/${id}`, data)
      .then(() => {
        setLoading(false);
        enqueueSnackbar('Movie Edited successfully', { variant: 'success' });
        navigate('/');
      })
      .catch((error) => {
        setLoading(false);
        enqueueSnackbar('Error', { variant: 'error' });
        console.log(error);
      });
  };

  return (
    <><nav className="bg-white border-gray-200 dark:bg-gray-900">
      <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <a href="/" className="flex items-center space-x-3 rtl:space-x-reverse">
          <img src="https://w7.pngwing.com/pngs/953/193/png-transparent-teros-minotaur-brawlhalla-demon-others-horse-purple-legendary-creature.png" className="h-8" alt="Logo" />
          <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">The Demonic Library</span>
        </a></div></nav><div className='p-4'>
        <BackButton />
        <h1 className='text-3xl my-4'>Edit Movie</h1>
        {loading ? <Spinner /> : ''}
        <div className='flex flex-col border-2 border-sky-400 rounded-xl w-[600px] p-4 mx-auto'>
        <div className='my-4'>
            <label className='text-xl mr-4 text-gray-500'>Title</label>
            <input
              type='text'
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              className='border-2 border-gray-500 px-4 py-2 w-full' />
          </div>
          <div className='my-4'>
            <label className='text-xl mr-4 text-gray-500'>description</label>
            <input
              type='text'
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              className='border-2 border-gray-500 px-4 py-2  w-full ' />
          </div>
          <div className='my-4'>
            <label className='text-xl mr-4 text-gray-500'>Genre</label>
            <input
              type='text'
              value={genre}
              onChange={(e) => setGenre(e.target.value)}
              className='border-2 border-gray-500 px-4 py-2 w-full' />
          </div>
          <div className='my-4'>
            <label className='text-xl mr-4 text-gray-500'>Release Year</label>
            <input
              type='text'
              value={ReleaseYear}
              onChange={(e) => setRY(e.target.value)}
              className='border-2 border-gray-500 px-4 py-2 w-full' />
          </div>
          <div className='my-4'>
            <label className='text-xl mr-4 text-gray-500'>Reviews</label>
            <input
              type='text'
              value={Reviews}
              onChange={(e) => setRw(e.target.value)}
              className='border-2 border-gray-500 px-4 py-2 w-full' />
          </div>
          <button className='p-2 bg-sky-300 m-8' onClick={handleEditBook}>
            Save
          </button>
        </div>
      </div></>
  )
}

export default EditBook