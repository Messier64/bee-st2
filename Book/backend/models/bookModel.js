import mongoose from 'mongoose';

const bookSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      maxLength: 255,
    },
    description: {
      type: String,
      required: true,
      maxLength: 1000,
    },
    genre: {
      type: String,
      required: true,
    },
    ReleaseYear:{
      type: Number,
      required: true,
    },
    Reviews:{
      type : Array , 
      "default" : []
    },
  },
  {
    timestamps: true,
  }
);

export const Book = mongoose.model('Book', bookSchema);
