# Project Structure

## Backend

- **book-store/backend/**
  - **models/**
  - **node_modules/**
  - **routes/**
  - **config.js**
  - **index.js**
  - **package-lock.json**
  - **package.json**

## Frontend

- **book-store/frontend/**
  - **node_modules/**
  - **public/**
  - **src/**
  - **.eslintrc.cjs**
  - **.gitignore**
  - **index.html**
  - **package-lock.json**
  - **package.json**
  - **postcss.config.js**
  - **tailwind.config.js**
  - **vite.config.js**

## Project Root

- **book-store/**
  - **.gitignore**
  - **README.md**

# Backend Dependencies

- **cors:** 2.8.5
- **express:** 4.18.2
- **mongoose:** 7.6.5
- **nodemon:** 2.0.22

# Frontend Dependencies

- **axios:** 1.4.0
- **notistack:** 3.0.1
- **react:** 18.2.0
- **react-dom:** 18.2.0
- **react-icons:** 4.10.1
- **react-router-dom:** 6.14.1
- **tailwindcss:** 3.3.2
- **vite:** 4.5.0
